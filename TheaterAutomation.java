package teatteri;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * 
 * @author Jarkko Laitinen
 * @date 4.3.2012
 * 
 * Suurimmaksi osaksi sama toteutus kun esimerkiss�, mutta vaihdoin kielen englanniksi. 
 * Threadien kanssa pieni� ongelma -> jonnekkin pit�isi laittaa lukko. mutta minne
 */

interface MessageBus {
    Message createMessage();

    void dismiss(Adapter adapter);

    void deliver(Message message);

    void add(Adapter adapter);
}

interface Message {
    void setType(String type);

    Object getType();

    void setMessage(String message);

    void deliver();

    String getMessage();
}

// AjaxV�yl�
class MediaBus implements MessageBus {

    private List<Adapter> adapters;
    private List<Message> messages;
    private int count;

    public MediaBus() {
        adapters = new ArrayList<Adapter>();
        messages = new ArrayList<Message>();
        count = 0;
    }

    @Override
    public Message createMessage() {
        int id = count;
        count++;
        return new MediaMessage(this, id);

        // bugi, miksi ei toimi return new MediaMessage(this, id++); ?
    }

    @Override
    public void add(Adapter adapter) {
        List<Message> delivered = new ArrayList<Message>();
        adapters.add(adapter);
        if (!messages.isEmpty()) {
            for (Message m : messages) {
                if (adapter.handleMessage(m))
                    delivered.add(m);
            }
            messages.removeAll(delivered);
        }
    }

    @Override
    public void deliver(Message message) {
        boolean done = false;
        for (Adapter adapter : adapters) {
            done = done || adapter.receive(message);
        }
        if (!done)
            messages.add(message);
    }

    @Override
    public void dismiss(Adapter adapter) {
        adapters.remove(adapter);
    }

}

// AjaxViesti
class MediaMessage implements Message {
    private int id;
    private String type;
    private String message;
    private MediaBus bus;

    public MediaMessage(MediaBus bus, int id) {
        this.bus = bus;
        this.id = id;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public void deliver() {
        bus.deliver(this);
    }

    @Override
    public Object getType() {
        return type;
    }

    public String toString() {
        return (id + ":" + type + ":" + message);
    }

    @Override
    public String getMessage() {
        return message;
    }
}

// Sovitin
abstract class Adapter {
    protected MessageBus bus;
    private List<String> types;

    public Adapter() {
        this.bus = null;
        this.types = new ArrayList<String>();
    }

    public void addType(String type) {
        types.add(type);
    }

    public boolean receive(Message message) {
        if (types.contains(message.getType()))
            return handleMessage(message);
        return false;
    }

    abstract public boolean handleMessage(Message message);

    public void joinBus(MessageBus v) {
        bus = v;
        bus.add(this);
    }

    public void quitBus(MessageBus v) {
        bus.dismiss(this);
        bus = null;
    }
}

interface SensorObserver {
    public void observation(String target);
}

class Sensor {

    private List<SensorObserver> observers;
    private String target;

    public Sensor(String target) {
        this.target = target;
        this.observers = new ArrayList<SensorObserver>();
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public void addObserver(SensorObserver target) {
        observers.add(target);
    }

    public void removeObserver(SensorObserver target) {
        observers.remove(target);
    }

    protected final void notifyObservers() {
        for (SensorObserver so : observers) {
            so.observation(target);
        }
    }
}

class DoorSensor extends Sensor implements Runnable {
    private int count = 0;

    public DoorSensor(String target) {
        super(target);
    }

    public void add() {
        this.count++;
        notifyObservers();
    }

    public int getCount() {
        return count;
    }

    @Override
    public void run() {
        add();
    }
}

class DoorSensorAdapter extends Adapter implements SensorObserver {
    private DoorSensor sensor;

    public DoorSensorAdapter(DoorSensor sensor) {
        addType("doorSensor");
        this.sensor = sensor;
        this.sensor.addObserver(this);
    }

    @Override
    public void observation(String target) {
        Message message = bus.createMessage();
        message.setType("doorSensorObservation");
        message.setMessage(target + " " + sensor.getCount());
        message.deliver();
    }

    @Override
    public boolean handleMessage(Message message) {
        System.out.println(this.toString() + "  " + message.toString());
        return false;
    }
}

class MediaSensor extends Sensor implements Runnable {
    public enum STATE {
        OFF, ON, PLAY, STOP, EJECT
    };

    private STATE state;

    public MediaSensor(String target) {
        super(target);
        state = STATE.OFF;
    }

    public STATE getState() {
        return this.state;
    }

    public void energize() {
        state = STATE.ON;
        notifyObservers();
    }

    public void startPlayback() {
        state = STATE.PLAY;
        notifyObservers();
    }

    public void stopPlayback() {
        state = STATE.STOP;
        notifyObservers();
    }

    public void ejectDisc() {
        state = STATE.EJECT;
        notifyObservers();
    }

    public void turnOff() {
        state = STATE.OFF;
        notifyObservers();
    }

    @Override
    public void run() {
        try {
            energize();
            Thread.sleep(100);
            startPlayback();
            Thread.sleep(100);
            stopPlayback();
            Thread.sleep(100);
            ejectDisc();
            Thread.sleep(100);
            turnOff();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class MediaSensorAdapter extends Adapter implements SensorObserver {
    private MediaSensor sensor;

    public MediaSensorAdapter(MediaSensor sensor) {
        addType("mediaSensor");
        this.sensor = sensor;
        this.sensor.addObserver(this);
    }

    @Override
    public void observation(String target) {
        MediaSensor.STATE state = sensor.getState();
        Message message = bus.createMessage();

        if (state == MediaSensor.STATE.OFF || state == MediaSensor.STATE.ON) {
            message.setType("powerNotification");
        } else {
            message.setType("playbackNotification");
        }
        message.setMessage(target + " " + state);
        message.deliver();
    }

    @Override
    public boolean handleMessage(Message message) {
        System.out.println(this.toString() + ": " + message.toString());
        return false;
    }

}

interface ControllableThingy {
    void addTarget(String target);
    boolean hasTarget(Object target);
    List<String> getTargets();
    void handleMessage(Message message);
}

class ControllableCurtains implements ControllableThingy {
    private List<String> targets;

    public ControllableCurtains() {
        targets = new ArrayList<String>();
    }
    @Override
    public void addTarget(String target) {
        targets.add(target);
    }
    @Override
    public boolean hasTarget(Object target) {
        return targets.contains(target);
    }
    @Override
    public List<String> getTargets() {
        return targets;
    }

    public void open() {
        System.out.println("Curtains open");
    }

    public void close() {
        System.out.println("Curtains close");
    }

    public void handleMessage(Message message) {

        StringTokenizer st = new StringTokenizer(message.getMessage(), " ");  
        st.nextToken();
        if (message.getType().equals("powerNotification")) {
            System.out.println("( Curtains received powerNotification )");
            MediaSensor.STATE state = MediaSensor.STATE.valueOf(st.nextToken());
            if (state.compareTo(MediaSensor.STATE.ON) == 0) {
                close();
            } else if (state.compareTo(MediaSensor.STATE.OFF) == 0) {
                open();
            }
        }
    }
}

class ControllableLights implements ControllableThingy {
    private List<String> targets;

    public ControllableLights() {
        targets = new ArrayList<String>();
    }

    @Override
    public void addTarget(String target) {
        targets.add(target);
    }

    @Override
    public boolean hasTarget(Object target) {
        return targets.contains(target);
    }

    @Override
    public List<String> getTargets() {
        return targets;
    }

    // if value % 2 != 0 going in, else going out
    private void react(int value) {
        if (value % 2 == 0) {
            turnOff();
        } else {
            turnOn();
        }
    }

    public void turnOn() {
        System.out.println("Lights on with full intensity");
    }

    public void dim() {
        System.out.println("Lights on with 50% intensity");
    }

    public void brighten() {
        System.out.println("Lights on with full intensity");
    }

    public void turnOff() {
        System.out.println("Lights out");
    }

    @Override
    public void handleMessage(Message message) {

        StringTokenizer st = new StringTokenizer(message.getMessage(), " ");
        st.nextToken();
        if (message.getType().equals("doorSensorObservation")) {
            System.out
                    .println("( ControllableLights received doorSensorObservation )");
            react(Integer.parseInt(st.nextToken()));
        } else if (message.getType().equals("powerNotification")
                || message.getType().equals("playbackNotification")) {
            System.out
                    .println("( ControllableLights received notification from MediaSensor )");
            MediaSensor.STATE state = MediaSensor.STATE.valueOf(st.nextToken());
            if (state.compareTo(MediaSensor.STATE.PLAY) == 0) {
                turnOff();
            } else if (state.compareTo(MediaSensor.STATE.EJECT) == 0) {
                turnOn();
            } else if (state.compareTo(MediaSensor.STATE.STOP) == 0
                    || state.compareTo(MediaSensor.STATE.ON) == 0) {
                dim();
            }
        }
    }
}

class LightAdapter extends Adapter {
    private ControllableLights light;

    public LightAdapter(ControllableLights lights) {
        this.light = lights;
        for (String type : lights.getTargets()) {
            addType(type);
        }
    }

    @Override
    public boolean handleMessage(Message message) {
        if (light.hasTarget(message.getType())) {
            light.handleMessage(message);
            return true;
        }
        return false;
    }
}

class CurtainAdapter extends Adapter {
    private ControllableCurtains curtain;

    public CurtainAdapter(ControllableCurtains curtain) {
        this.curtain = curtain;
        for (String type : curtain.getTargets()) {
            addType(type);
        }
    }

    @Override
    public boolean handleMessage(Message message) {
        if (curtain.hasTarget(message.getType())) {
            curtain.handleMessage(message);
            return true;
        }
        return false;
    }
}

public class TheaterAutomation {

    public static void main(String[] args) {
        MessageBus bus = new MediaBus();

        DoorSensor ds = new DoorSensor("door");
        DoorSensorAdapter dsa = new DoorSensorAdapter(ds);

        // Lights
        ControllableLights lights = new ControllableLights();
        lights.addTarget("doorSensorObservation");
        lights.addTarget("playbackNotification");
        lights.addTarget("powerNotification");
        LightAdapter la = new LightAdapter(lights);

        // Curtains
        ControllableCurtains curtains = new ControllableCurtains();
        curtains.addTarget("powerNotification");
        CurtainAdapter ca = new CurtainAdapter(curtains);

        // Media
        MediaSensor ms = new MediaSensor("device");
        MediaSensorAdapter msa = new MediaSensorAdapter(ms);

        ca.joinBus(bus);
        la.joinBus(bus);

        msa.joinBus(bus);
        dsa.joinBus(bus);

        Thread s = new Thread(ds);
        Thread a = new Thread(ms);
        s.start();
        a.start();
        

    }
}