package teatteri;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

interface Autolaskuri {
    void lisääYhdellä();
    void nollaa();
    int getLkm();
    String havainnoitava();
}

public class AutolaskuriInterface {
    
    //Tehdään kaikki listas singletoneina.
    private static ArrayList<Autolaskuri> laskurit;
    private static HashMap<String, Integer> arvot;
    private static Set<String> avaimet;
    
    //Tehtävänannossa pyydettiin "lataamaan" pluginit jostain
    //Nämä pystyttäisiin lataaamaan toisista tiedostoista myös.
    private ArrayList<Autolaskuri> loadPlugins() {
        laskurit = new ArrayList<Autolaskuri>();
        laskurit.add(new LentokoneLaskuri());
        laskurit.add(new Laivalaskuri());
        laskurit.add(new Autolaskurit());
        return laskurit;
    }
       
    private void lisääYhdellä() {
        for (Autolaskuri laskuri : laskurit) {
            laskuri.lisääYhdellä();
        }
    }
    
    private void nollaa() {
        for (Autolaskuri laskuri : laskurit) {
            laskuri.nollaa();
        }
    }
    
    private HashMap<String, Integer> getLkm() {
       HashMap<String, Integer> esiintymät = new HashMap<String, Integer>();
       for (Autolaskuri laskuri : laskurit){
           esiintymät.put(laskuri.havainnoitava(), laskuri.getLkm());
       }
       return esiintymät;
    }
    
    public void tulostaArvot(){
        arvot = getLkm();
        avaimet = arvot.keySet();
        for (String avain : avaimet){
            System.out.println(avain + " " + arvot.get(avain));
        }
    }
    
    public static void main(String[] args){
        AutolaskuriInterface ai = new AutolaskuriInterface();
        HashMap<String, Integer> arvot;
        Set<String> avaimet;
        ArrayList<Autolaskuri> laskurit = ai.loadPlugins();
        
        System.out.println("Ennen lisäystä arvot ovat: ");
        ai.tulostaArvot();
        ai.lisääYhdellä();
        System.out.println("Arvojen lisäämisen jälkeen");        
        ai.tulostaArvot();
        System.out.println("Nollataan arvot");
        ai.nollaa();
        ai.tulostaArvot();
    }
    
    
}

class LentokoneLaskuri implements Autolaskuri {
    
    private String havainnoitava = "Lentokoneita ";
    private int lkm = 0;
    
    @Override
    public void lisääYhdellä() {
        lkm++;
    }

    @Override
    public void nollaa() {
        lkm = 0;
    }

    @Override
    public int getLkm() {
        return lkm;
    }

    @Override
    public String havainnoitava() {
        return havainnoitava;
    }
    
}


class Laivalaskuri implements Autolaskuri {
    private String havainnoitava = "Laivoja ";
    private int lkm = 0;
    
    @Override
    public void lisääYhdellä() {
        lkm++;
    }
    
    @Override
    public void nollaa() {
        lkm = 0;
    }
    
    @Override 
    public int getLkm() {
        return lkm;
    }
    
    @Override
    public String havainnoitava() {
        return havainnoitava;
    }
}

class Autolaskurit implements Autolaskuri {
    private String havainnoitava = "Autoja ";
    private int lkm = 0;
    
    @Override
    public void lisääYhdellä() {
        lkm++;
    }
    
    @Override 
    public void nollaa() {
        lkm = 0;
    }
    
    @Override
    public int getLkm() {
        return lkm;
    }
    
    @Override
    public String havainnoitava() {
        return havainnoitava;
    }
}
